package com.example.androidtesttask.view;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.example.androidtesttask.R;
import com.example.androidtesttask.data.Point;
import com.example.androidtesttask.databinding.MainActivityDataBinding;
import com.example.androidtesttask.viewmodel.PointsViewModel;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Integer.valueOf;
import static java.lang.String.format;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "GVIDON";

    private MainActivityDataBinding mBinding;
    private PointsViewModel pointsViewModel;
    private LineChart chart;
    
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBinding.setLifecycleOwner(this);
        pointsViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(this.getApplication()).create(PointsViewModel.class);
        mBinding.setPointsViewModel(pointsViewModel);
        chart = mBinding.lineChart;
        setUpChartUi();
        pointsViewModel.getCountOfPoints().observe(this, count -> {
            if (!count.isEmpty()) {
                pointsViewModel.getAllPoints(valueOf(count)).observe(this, points -> {
                    if (points.size() > 0) {
                        chart.setData(getChartData(points));
                        updateChart();
                    }
                });
                pointsViewModel.getErrors().observe(this, error -> {
                    invalidateChart();
                    setMessageToCart(format("There is an error \"%s\"", error));
                });
            } else {
                invalidateChart();
            }
        });
    }

    private void updateChart(){
        chart.animateXY(2000, 2000);
        chart.invalidate();
    }

    private void setUpChartUi() {
        setMessageToCart(getString(R.string.no_data_chart_string));
        chart.setViewPortOffsets(0, 0, 0, 0);
        chart.getDescription().setEnabled(false);
        chart.setTouchEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setPinchZoom(false);
        chart.setDrawGridBackground(false);
        chart.getAxisRight().setEnabled(false);
        chart.getLegend().setEnabled(false);
        XAxis x = chart.getXAxis();
        YAxis y = chart.getAxisLeft();
        x.setEnabled(false);
        y.setLabelCount(6, false);
        y.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        y.setDrawGridLines(false);
        updateChart();
    }

    private LineData getChartData(List<Point> points) {
        Collections.sort(points, (point1, point2) -> point1.x.compareTo(point2.x));
        List<Entry> entries = new ArrayList<>();
        for (Point point : points) {
            Entry e = new Entry();
            e.setX(point.getX());
            e.setY(point.getY());
            entries.add(e);
        }
        LineDataSet lineDataSet = new LineDataSet(entries, "Points");
        lineDataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        lineDataSet.setDrawCircles(true);
        lineDataSet.setCircleColor(Color.BLACK);
        lineDataSet.setLineWidth(1f);
        lineDataSet.setColor(Color.MAGENTA);
        LineData data = new LineData(lineDataSet);
        data.setValueTextSize(5f);
        return data;
    }

    private void invalidateChart() {
        mBinding.lineChart.clear();
        mBinding.lineChart.invalidate();
    }

    private void setMessageToCart(String message) {
        mBinding.lineChart.setNoDataText(message);
    }

}
